// interface autocloseable
// implementuje ze powinnismy miec zamkniecie metod
// pojawia sie error i klikamy i close dzieki temu

class Temp implements AutoCloseable {

	@Override
	public void close() throws Exception {

		System.out.println("Closing!");
		throw new Exception("oh no!");
	}

}

public class App {

	public static void main(String[] args) {

		// zamiast try catch czyli to co na dole
		// robimy tzw try with resources
		// czyli to co ponizej
		// sposob2
		// dodaje catch clause po kliknieciu w error
		// close idzie z automatu
		try (Temp temp = new Temp()) {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// musze zamknac bo bedzie babol
		// sposob 1 try catch
		/*
		 * try { temp.close(); } catch (Exception e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 */

	}

}
