import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class App2 {

	public static void main(String[] args) {

		// sposob stary - przed java 7
		// tak ten file reader sie opisywalo
		// teraz inaczej zeby bylo prosciej
		// File file = new File("test.txt");
		// FileReader fr = new File Reader(file);
		// BufferedReader br = new BufferedReader(fr);

		// nowa szkola
		// mniej pisania
		// i tez uzyjemy try with resources
		// teraz jest prosciej niz ten nested try catch z poprzedniego tutoriala
		File file = new File("test.txt");
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

		} catch (FileNotFoundException e) {
			System.out.println("Can't find file " + file.toString());

		} catch (IOException e) {
			System.out.println("Unable to read file " + file.toString());
		}

	}

}
